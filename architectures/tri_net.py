#!coding:utf-8
import torch
import torch.nn as nn
import torch.nn.functional as F

class CNN_block(nn.Module):

    def __init__(self, in_plane, out_plane, kernel_size, padding, activation):
        super(CNN_block, self).__init__()

        self.act = activation
        self.conv = nn.Conv2d(in_plane, 
                              out_plane, 
                              kernel_size, 
                              padding=padding)

        self.bn = nn.BatchNorm2d(out_plane)

    def forward(self, x):
        return self.act(self.bn(self.conv(x)))

class PreActBlock(nn.Module):
    expansion = 1
    
    def __init__(self, in_planes, planes, stride=1):
        super(PreActBlock, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_planes)
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)

        if stride!=1 or in_planes!=self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False)
            )

    def forward(self, x):
        out = F.relu(self.bn1(x))
        shortcut = self.shortcut(out) if hasattr(self, 'shortcut') else x
        out = self.conv1(out)
        out = self.conv2(F.relu(self.bn2(out)))
        out += shortcut
        return out

class ConvModule(nn.Module):

    def __init__(self, in_plane, kernel_size, pads, activation, num_classes=10):
        super(ConvModule, self).__init__()

        self.in_plane = in_plane
        self.act = activation
        self.layer1 = self.__make_layer(CNN_block, 3, 256, kernel_size[0], pad=pads[0])
        self.mp1 = nn.MaxPool2d(2, stride=2, padding=0)
        self.drop1 = nn.Dropout(0.5)
        self.layer2 = self.__make_layer(CNN_block, 3, 512, [kernel_size[1],1,1], pad=pads[1])
        self.mp2 = nn.MaxPool2d(2, stride=2, padding=0)
        self.drop2 = nn.Dropout(0.5)
        self.fc = nn.Linear(self.in_plane, num_classes)

    def __make_layer(self, block, num_blocks, planes, kernel_size, pad=1):
        if isinstance(planes, int):
            planes = [planes]*num_blocks
        if isinstance(kernel_size, int):
            kernel_size = [kernel_size]*num_blocks
        layers = []
        for plane, ks in zip(planes, kernel_size):
            padding = 0 if ks==1 else pad
            layers.append(block(self.in_plane, plane, ks, padding, self.act))
            self.in_plane = plane
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.layer1(x)
        out = self.drop1(self.mp1(out))
        out = self.layer2(out)
        out = self.drop2(self.mp2(out))
        out = F.avg_pool2d(out, out.size()[2:])
        out = out.view(out.size(0), -1)
        return self.fc(out)

class ResModule(nn.Module):

    def __init__(self, in_plane, activation, num_classes=10):
        super(ResModule, self).__init__()

        self.act = activation
        self.res1 = PreActBlock(in_plane, 256, stride=1)
        self.mp1 = nn.MaxPool2d(2, stride=2, padding=0)
        self.drop1 = nn.Dropout(0.5)
        self.res2 = PreActBlock(256, 512, stride=1)
        self.mp2 = nn.MaxPool2d(2, stride=2, padding=0)
        self.drop2 = nn.Dropout(0.5)
        self.conv1 = CNN_block(512, 512, kernel_size=1, padding=0, activation=self.act)
        self.conv2 = CNN_block(512, 512, kernel_size=1, padding=0, activation=self.act)
        self.fc = nn.Linear(512, num_classes)

    def forward(self, x):
        out = self.res1(x)
        out = self.drop1(self.mp1(out))
        out = self.res2(out)
        out = self.drop2(self.mp2(out))
        out = F.avg_pool2d(out, out.size()[2:])
        out = out.view(out.size(0), -1)
        return self.fc(out)


class TriNet(nn.Module):

    def __init__(self, in_plane, num_classes=10):
        super(TriNet, self).__init__()

        out_plane = 128
        self.act = nn.LeakyReLU(0.1)
        self.MS = nn.Sequential(
            CNN_block(in_plane, out_plane, kernel_size=3, padding=1, activation=self.act),
            CNN_block(out_plane, out_plane, kernel_size=3, padding=1, activation=self.act),
            CNN_block(out_plane, out_plane, kernel_size=3, padding=1, activation=self.act),
            nn.MaxPool2d(2, stride=2, padding=0)
        )
        self.M1 = ConvModule(out_plane, [5,5], [2,1], self.act, num_classes)
        self.M2 = ConvModule(out_plane, [3,3], [1,0], self.act, num_classes)
        self.M3 = ResModule(out_plane, self.act, num_classes)

    def forward(self, x):
        out = self.MS(x)
        out1 = self.M1(out)
        out2 = self.M2(out)
        out3 = self.M3(out)
        return out1, out2, out3

def triNet(num_classes):
    return TriNet(3, num_classes)

def test():
    print('--- run tri-net test ---')
    x = torch.randn(2,3,32,32)
    for net in [triNet(10)]:
        print(net)
        y1, y2, y3 = net(x)
        print(y1.size())
        print(y2.size())
        print(y3.size())
