#!coding:utf-8
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils import weight_norm
from torch.nn import ConvTranspose2d as ConvT2d

#from architectures.arch import RegisterArch

class GaussianNoise(nn.Module):
    
    def __init__(self, std):
        super(GaussianNoise, self).__init__()
        self.std = std

    def forward(self, x):
        zeros = torch.zeros(x.size()).cuda()
        n = Variable(torch.normal(zeros_, std=self.std).cuda())
        return x + n

class CNN_block(nn.Module):

    def __init__(self, in_plane, out_plane, kernel_size, padding, activation):
        super(CNN_block, self).__init__()

        self.act = activation
        self.conv = nn.Conv2d(in_plane, out_plane, kernel_size, 
                              padding=padding)
        self.bn = nn.BatchNorm2d(out_plane)

    def forward(self, x):
        return self.act(self.bn(self.conv(x)))

class TCNN_block(nn.Module):

    def __init__(self, in_plane, out_plane, kernel_size, padding, activation):
        super(TCNN_block, self).__init__()

        self.act = activation
        self.conv = ConvT2d(in_plane, out_plane, kernel_size, 
                            padding=padding)
        self.bn = nn.BatchNorm2d(out_plane)

    def forward(self, x):
        return self.act(self.bn(self.conv(x)))


class CNN(nn.Module):

    def __init__(self, block, tblock, num_blocks, num_classes=10):
        super(CNN, self).__init__()

        self.gn  = GaussianNoise(0.15)
        self.act = nn.LeakyReLU(0.1)

        ## Encoder
        self.in_plane  = 3
        self.out_plane = 128
        self.layer1 = self._make_layer(block, num_blocks[0], 128, 3,
                                       padding=1)
        self.mp1    = nn.MaxPool2d(2, stride=2, padding=0,
                                   return_indices=True)
        #self.drop1  = nn.Dropout(0.5)
        self.layer2 = self._make_layer(block, num_blocks[1], 256, 3,
                                       padding=1)
        self.mp2    = nn.MaxPool2d(2, stride=2, padding=0,
                                   return_indices=True)
        #self.drop2  = nn.Dropout(0.5)
        self.layer3 = self._make_layer(block, num_blocks[2], 
                                       [512, 256, self.out_plane], 
                                       [3, 1, 1], 
                                       padding=0)
        self.ap3 = nn.AdaptiveAvgPool2d(1)
        self.fc1 = weight_norm(nn.Linear(self.out_plane, num_classes))
        #self.fc1 = nn.Linear(self.out_plane, num_classes)

        ## Decoder
        self.in_plane  = 128
        self.out_plane = 3
        self.tlayer1 = self._make_layer(tblock, num_blocks[2], 
                                       [256, 512, 256], 
                                       [1, 1, 3], 
                                       padding=0)
        self.ump1    = nn.MaxUnpool2d(2, stride=2, padding=0)
        self.tlayer2 = self._make_layer(tblock, num_blocks[1],
                                       [256, 256, 128],
                                       kernel_size=3,
                                       padding=1)
        self.ump2    = nn.MaxUnpool2d(2, stride=2, padding=0)
        self.tlayer3 = self._make_layer(tblock, num_blocks[1],
                                       [128, 128, self.out_plane],
                                       kernel_size=3,
                                       padding=1)
      
    def _make_layer(self, block, num_blocks, planes, kernel_size,
                    padding=1):
        if isinstance(planes, int):
            planes = [planes]*num_blocks
        if isinstance(kernel_size, int):
            kernel_size = [kernel_size]*num_blocks
        layers = []
        for plane, ks in zip(planes, kernel_size):
            layers.append(block(self.in_plane, plane, ks, padding, self.act))
            self.in_plane = plane
        return nn.Sequential(*layers)

    def forward(self, x):
        ##=== encodera ===
        x = self.layer1(x)
        x, idx1 = self.mp1(x)
        x = self.layer2(x)
        x, idx2 = self.mp2(x)
        x = self.layer3(x)
        ## classifier
        out = self.ap3(x)
        out = out.view(out.size(0), -1)
        ##=== decoder ===
        x = self.tlayer1(x)
        x = self.ump1(x, idx2)
        x = self.tlayer2(x)
        x = self.ump2(x, idx1)
        x = self.tlayer3(x)
        return self.fc1(out), x


def convLarge_rec(num_classes):
    return CNN(CNN_block, TCNN_block, [3,3,3], num_classes)

def test():
    print('--- run conv_large test ---')
    x = torch.randn(2,3,32,32)
    for net in [convLarge_rec(10)]:
        print(net)
        y,x_rec = net(x)
        print(y.size())
        print(x_rec.size())
