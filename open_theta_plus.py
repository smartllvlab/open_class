#!coding:utf-8
import os
import random
import numpy as np
from itertools import chain
from random import choices

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.optim import lr_scheduler
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler

import torchvision
import torchvision.transforms as transforms

from utils import datasets
from utils.ramps import exp_warmup
from utils.metrics import roc
from utils.metrics import select_theta
from utils.metrics import evaluation
from utils.metrics import bi_evaluation
from utils.metrics import area_under_roc
from utils.data_utils import TransformTwice as twice
from utils.data_utils import TwoStreamBatchSamplerFix, TwoStreamBatchSampler
from utils.data_utils import relabel_dataset_close, relabel_test_dataset_open_plus
from utils.data_utils import relabel_test_dataset_close
from utils.data_utils import relabel_dataset_open_plus
from trainer import consTrainer
from trainer import piTrainer
from trainer import supTrainer
from architectures.arch import arch

from utils.data_utils import NO_LABEL

cifar10_animal = ['bird', 'cat', 'deer', 'dog', 'frog', 'horse']
cifar10_nonanimal = ['airplane', 'automobile', 'ship', 'truck']

cifar100_nonanimal = ['bottle', 'bowl', 'can', 'cup', 'plate',
              'clock', 'keyboard', 'lamp', 'telephone', 'television',
              'bed', 'chair', 'couch', 'table', 'wardrobe',
              'bridge', 'castle', 'house', 'road', 'skyscraper',
              'cloud', 'forest', 'mountain', 'plain', 'sea',
              'bicycle', 'bus', 'motorcycle', 'pickup_truck', 'train',
              'lawn_mower', 'rocket', 'streetcar', 'tank', 'tractor']

cifar100_animal = ['beaver', 'dolphin', 'otter', 'seal', 'whale',
              'bee', 'beetle', 'butterfly', 'caterpillar', 'cockroach',
              'bear', 'leopard', 'lion', 'tiger', 'wolf',
              'camel', 'cattle', 'chimpanzee', 'elephant', 'kangaroo',
              'fox', 'porcupine', 'possum', 'raccoon', 'skunk',
              'aquarium_fish', 'flatfish', 'ray', 'shark', 'trout',
              'crab', 'lobster', 'snail', 'spider', 'worm',
              'crocodile', 'dinosaur', 'lizard', 'snake', 'turtle',
              'baby', 'boy', 'girl', 'man', 'woman',
              'hamster', 'mouse', 'rabbit', 'shrew', 'squirrel']

def create_data_loaders(train_transform, 
                        eval_transform, 
                        datadir,
                        num_classes,
                        config):
    ## set-up two stream dataloader
    if config.twice:
        train_transform = twice(train_transform)
        eval_transform = twice(eval_transform)

    traindir = os.path.join(datadir, config.train_subdir)
    trainset = torchvision.datasets.ImageFolder(traindir, train_transform)
    ## construct close-set dict
    close_class = cifar10_nonanimal
    if config.open == 6:
        open_class = cifar10_animal
        config.prior = 0.4
    elif config.open == 0:
        open_class = cifar100_animal
        config.prior = 18400./(18400+ 500*50)
    else:
        open_class = choices(cifar100_animal, k=config.open)
        config.prior = 18400./(18400+ 500*config.open)
    c2i = trainset.class_to_idx
    close_set_dict = {c2i[c]:i for i, c in enumerate(close_class)}    
    unlabeled_set = {c2i[c] for c in chain(close_class, open_class) }
    print('the number of unlabeled set: ', len(unlabeled_set))
    ## relabel dataset
    if config.labels:
        with open(config.labels) as f:
            labels = dict(line.split(' ') for line in f.read().splitlines())
        #labeled_idxs, unlabeled_idxs = relabel_dataset_close(trainset, labels, close_set_dict)
        labeled_idxs, unlabeled_idxs = relabel_dataset_open_plus(trainset, labels,
                                                                 close_set_dict,
                                                                 unlabeled_set)
    #assert len(trainset.imgs) == len(labeled_idxs)+len(unlabeled_idxs)
    print("close-set map: ", close_set_dict)
    select_class = {v:k for k,v in trainset.class_to_idx.items()
                        if v in close_set_dict}
    print("close-set class: ", select_class)
    print("total training samples: ", len(trainset.imgs))
    print("labeled samples: ", len(labeled_idxs))
    print("unlabeled samples: ", len(unlabeled_idxs))
    if config.labeled_batch_size < config.batch_size:
        assert len(unlabeled_idxs)>0
        if config.model=='tempens':
            batchSampler = TwoStreamBatchSamplerFix
        else:
            batchSampler = TwoStreamBatchSampler
        batch_sampler = batchSampler(
            unlabeled_idxs, labeled_idxs, config.batch_size, config.labeled_batch_size)
    else:
        print("using labeled data only")
        sampler = SubsetRandomSampler(labeled_idxs)
        batch_sampler = BatchSampler(sampler, config.batch_size, drop_last=True)
    train_loader = torch.utils.data.DataLoader(trainset,
                                               batch_sampler=batch_sampler,
                                               num_workers=config.workers,
                                               pin_memory=True)
    ## load testset dir
    evaldir = os.path.join(datadir, config.eval_subdir)

    ## relabel close test dataset
    evalset = torchvision.datasets.ImageFolder(evaldir,eval_transform)
    eval_idxs = relabel_test_dataset_close(evalset, close_set_dict)
    print("test known samples: ", len(eval_idxs))
    eval_sampler = SubsetRandomSampler(eval_idxs)
    eval_batch_sampler = BatchSampler(eval_sampler, config.batch_size, drop_last=False)
    eval_loader  = torch.utils.data.DataLoader(evalset,
                                               batch_sampler=eval_batch_sampler,
                                               num_workers=2*config.workers,
                                               pin_memory=True)

    ## relabel open test dataset
    openset = torchvision.datasets.ImageFolder(evaldir,eval_transform)
    eval_open_idxs = relabel_test_dataset_open_plus(openset, close_set_dict, unlabeled_set)
    print("test samples: ", len(eval_open_idxs))
    print("prior: ", config.prior)
    eval_open_sampler = SubsetRandomSampler(eval_open_idxs)
    eval_open_batch_sampler = BatchSampler(eval_open_sampler, config.batch_size, drop_last=False)
    open_loader = torch.utils.data.DataLoader(openset,
                                              batch_sampler=eval_open_batch_sampler,
                                              num_workers=2*config.workers,
                                              pin_memory=True)
    return train_loader, eval_loader, open_loader, close_set_dict, len(unlabeled_idxs)

def create_loss_fn(config):
    if config.loss == 'mse':
        criterion = nn.mseloss()
    elif config.loss == 'soft':
        # for pytorch 0.4.1 and 1.0.0
        criterion = nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduction='none')
        # for pytorch 0.4.0
        #criterion = nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduce=False)
    return criterion

def create_optim(params, config):
    if config.optim == 'sgd':
        optimizer = optim.SGD(params, config.lr,
                              momentum=config.momentum,
                              weight_decay=config.weight_decay,
                              nesterov=config.nesterov)
    elif config.optim == 'adam':
        optimizer = optim.Adam(params, config.lr)
    return optimizer

def create_lr_scheduler(optimizer, config):
    if config.lr_scheduler == 'cos':
        scheduler = lr_scheduler.CosineAnnealingLR(optimizer,
                                                   T_max=config.epochs,
                                                   eta_min=config.min_lr)
    elif config.lr_scheduler == 'multistep':
        if config.steps=="":
            return None
        scheduler = lr_scheduler.MultiStepLR(optimizer,
                                             milestones=config.steps,
                                             gamma=config.gamma)
    elif config.lr_scheduler == 'exp-warmup':
        lr_lambda = exp_warmup(config.rampup_length,
                               config.rampdown_length,
                               config.epochs)
        scheduler = lr_scheduler.LambdaLR(optimizer,
                                          lr_lambda=lr_lambda)
    elif config.lr_scheduler == 'none':
        scheduler = None
    return scheduler

def main_test(config):
    dataset_config = datasets.load_data[config.dataset]()
    ret = create_data_loaders(**dataset_config, config=config)
    train_loader, eval_loader, open_loader, close_set_dict, unlabeled_size = ret
    
    print('-'*50, 'train')
    for (x1, x2), y in train_loader:
        print(x1.size()) 
        print(x2.size()) 
        print(y.size()) 
        print(y) 
        break

    print('-'*50, 'test')
    for (x1, x2), y in eval_loader:
        print(x1.size()) 
        print(x2.size()) 
        print(y.size()) 
        print(y) 
        break

    print(close_set_dict)


def main(config):
    print("pytorch version : {}".format(torch.__version__))
    dataset_config = datasets.load_data[config.dataset]()
    ret = create_data_loaders(**dataset_config, config=config)
    train_loader, eval_loader, open_loader, close_set_dict, unlabeled_size = ret

    num_classes = len(close_set_dict)
    open_label  = len(close_set_dict)
    net = arch[config.arch](num_classes)

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    criterion = create_loss_fn(config)
    net = net.to(device)
    optimizer = create_optim(net.parameters(), config)
    scheduler = create_lr_scheduler(optimizer, config)

    ##=== setup trainer ===
    if config.model == 'ema':
        net2 = arch[config.arch](num_classes)
        net2 = net2.to(device)
        trainer = consTrainer.Trainer(net, net2, optimizer, criterion, device, config)
    elif config.model == 'pi':
        trainer = piTrainer.Trainer(net, net, optimizer, criterion, device, config)
    elif config.model == 'sup':
        trainer = supTrainer.Trainer(net, net, optimizer, criterion, device, config)
    #trainer.loop(config.epochs, train_loader, eval_loader, scheduler=scheduler)

    ##=== main ===
    best_acc, best_f1 = 0., 0.
    for ep in range(config.epochs):
        if scheduler is not None:
            scheduler.step()
        print("------ Training epochs: {} ------".format(ep))
        trainer.train(train_loader, config.print_freq)
        ## can not run the test
        #print("------ Testing epochs: {} ------".format(ep))
        #trainer.test(eval_loader, config.print_freq)
        print("------ Testing open epochs: {} ------".format(ep))
        labels, preds, probs = trainer.obtain_prediction(open_loader)

        ## set probs below theta as open
        theta = select_theta(probs, config.prior)
        close_acc,_ = evaluation(labels, preds, open_label)
        print(f"close-set acc: {close_acc:.3%}")
        bpreds = preds.clone()
        bpreds[probs < theta] = open_label
        print("--- labels ---")
        for i in labels.unique():
            print(i, labels.eq(i).sum())
        print("--- preds ---")
        for i in bpreds.unique():
            print(i, bpreds.eq(i).sum())

        acc, f1_score = evaluation(labels, bpreds, open_label)
        bi_acc, bi_err = bi_evaluation(labels, bpreds, open_label)
        auc = area_under_roc(labels, probs, open_label)

        ## save the prediction
        if (ep+1)%100==0:
            ## mean acc
            fpr, tpr, thres = roc(labels, probs, open_label)
            accs = thres.copy()
            baccs = thres.copy()
            f1s = thres.copy()
            bacc, bf1, bbi_acc = 0.,0.,0.
            for i, t in enumerate(thres):
                t = float(t)
                mpreds = preds.clone()
                mpreds[probs < t] = open_label
                macc, mf1 = evaluation(labels, mpreds, open_label)
                mbi_acc, _ = bi_evaluation(labels, mpreds, open_label)
                accs[i] = macc
                baccs[i] = mbi_acc
                f1s[i] = mf1
                if mf1 > bf1:
                    bacc, bf1, bbi_acc = macc, mf1, mbi_acc
            print(f'max acc: {np.max(accs):.3%}\t mean acc: {np.mean(accs):.3%}\t best acc: {bacc:.3%}\n'\
                  f'max f1: {np.max(f1s):.3%}\t mean f1: {np.mean(f1s):.3%}\t best f1: {bf1:.3%}\n'\
                  f'max biAcc: {np.max(baccs):.3%}\t mean biAcc: {np.mean(baccs):.3%}\t best biAcc: {bbi_acc:.3%}')

            f1 = 'tmp/{}_{}_theta_prediction_{}'.format(config.model, config.dataset, ep)
            np.savez(f1, labels.cpu().numpy(), probs.cpu().numpy(), bpreds.cpu().numpy(), preds.cpu().numpy())

            f2 = 'tmp/{}_{}_theta_roc_curve_{}'.format(config.model, config.dataset, ep)
            np.savez(f2, fpr, tpr, thres)
        
        ## log
        if f1_score > best_f1:
            best_acc, best_f1 = acc, f1_score
        print(f"[test]Theta: {theta:.5}\t Acc: {acc:.3%}\t Open score: {f1_score:.3%}\t BiAcc: {bi_acc:.3%} \t AUC: {auc:.3%}")
        print(f"[best]Acc: {best_acc:.3%}\t Open score: {best_f1:.3%}")
        ## save model
        if config.save_freq!=0 and (ep+1)%config.save_freq == 0:
            trainer.save(ep)
    
    #print("------ Open Test over all threshold ------")
    #labels, preds, probs = trainer.obtain_prediction(open_loader)
    #thetas = (x*0.01 for x in range(100))
    #for theta in thetas:
    #    preds_tmp = preds.clone()
    #    preds_tmp[probs < theta] = open_label
    #    acc, f1_score = evaluation(labels, preds_tmp, open_label)
    #    print(f"[theta: {theta:.3f}]Acc: {acc:.3%}\t Open score: {f1_score:.3%}")
    

if __name__ == '__main__':
    from util.Config import create_parser
    config = create_parser().parse_args()
    main(config)
