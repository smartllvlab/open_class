import sys
import torch

import nnpu_ict
from utils.Config import parse_dict_args

def parameters():
    defaults = {
        # Log and save
        'print_freq': 30,
        'save_freq': 100,
        'save_dir': 'checkpoints/',

        # Technical details
        'is_parallel': False,
        'workers': 2,

        # Data
        #'dataset': 'cinic10',
        #'dataset': 'cifar10',
        'dataset': 'svhn',
        'base_batch_size': 100,
        'base_labeled_batch_size': 50,
        'train_subdir': 'train+val',
        #'train_subdir': 'train',
        'eval_subdir': 'test',

        # Architecture
        #'arch': 'lenet',
        #'arch': 'vgg19',
        #'arch': 'preact_resnet18',
        #'arch': 'densenet121',
        #'arch': 'resnext29_32x4d',
        #'arch': 'senet',
        #'arch': 'dpn92',
        #'arch': 'shuffleG3',
        #'arch': 'mobileV2',
        #'arch': 'convlarge',
        #'arch': 'split',
        #'arch': 'convlarge_rec',
        #'arch': 'convlarge',
        'arch': 'resnet18',
        'model': 'nnpu',

        # Optimization
        'loss': 'soft',
        'optim': 'adam',
        'epochs': 100,
        'base_lr': 0.001,
        'momentum': 0.9,
        'weight_decay': 5e-4,
        'nesterov': True,
        'cons_loss_type': 'mse',

        # lr_schedular
        'steps': '100,150,200,250,300,350,400,450,480',
        'gamma': 0.5,
        'lr_scheduler': 'cos',#'exp-warmup',
        #'lr_scheduler': 'exp-warmup',
        'min_lr': 1e-4,
        'rampup_length': 80,
        'rampdown_length': 50,
        
        # MeanTeacher
        'cons_weight': 30.0,
        'ema_decay': 0.97,
        'twice': True,
        'weight': 1.0,
        
        # Open
        'close': 6,
        'open': 10,
        'theta': 0.4,
    }

    defaults['save_dir'] += '{}/'.format(defaults['model'])
    #for n_labels in [1000]:
    #for ds in ['svhn', 'mnist', 'cifar10']:
    #for ds in ['tiny-imagenet']:
    #for ds in ['cifar10_plus']:
    for ds in ['cifar10']:
        defaults['dataset'] = ds
        if defaults['dataset'] in ['svhn', 'mnist']:
            n_labels = 1000
        elif defaults['dataset'] == 'cifar10':
            n_labels = 4000
        elif defaults['dataset'] == 'tiny-imagenet':
            n_labels = 20000
            defaults['close'] = 40
        elif defaults['dataset'] == 'cifar10_plus':
            n_labels = 4000
        for data_seed in range(10, 11):
            yield{
                **defaults,
                'n_labels': n_labels,
                'data_seed': data_seed
            }

def run(base_batch_size, base_labeled_batch_size, base_lr, n_labels, data_seed, is_parallel, **kwargs):
    if is_parallel and torch.cuda.is_available():
        ngpu = torch.cuda.device_count()
    else:
        ngpu = 1
    adapted_args = {
        'batch_size': base_batch_size * ngpu,
        'labeled_batch_size': base_labeled_batch_size * ngpu,
        'lr': base_lr,
        'labels': 'data-local/labels/{}/{}_balanced_labels/{:02d}.txt'.format(kwargs['dataset'], n_labels, data_seed),
        'is_parallel': is_parallel,
    }
    args = parse_dict_args(**adapted_args, **kwargs)
    nnpu_ict.main(args)


if __name__ == "__main__":
    for run_params in parameters():
        run(**run_params)
