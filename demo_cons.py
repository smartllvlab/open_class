#!coding:utf-8
import os
import random

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.optim import lr_scheduler
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler

import torchvision
import torchvision.transforms as transforms

from utils import datasets
from trainer import consTrainer
from trainer import consDBTrainer
from trainer import piTrainer
from utils.data_utils import TransformTwice as twice
from utils.data_utils import TwoStreamBatchSamplerFix, TwoStreamBatchSampler
from utils.data_utils import relabel_dataset, relabel_dataset_close, relabel_test_dataset_close
from utils.ramps import exp_warmup
from architectures.arch import arch
from architectures.conv_large import *

from utils.data_utils import NO_LABEL

def create_data_loaders(train_transform, 
                        eval_transform, 
                        datadir,
                        num_classes,
                        config):
    if config.twice:
        train_transform = twice(train_transform)
        eval_transform = twice(eval_transform)
    traindir = os.path.join(datadir, config.train_subdir)
    trainset = torchvision.datasets.ImageFolder(traindir, train_transform)
    if config.labels:
        with open(config.labels) as f:
            labels = dict(line.split(' ') for line in f.read().splitlines())
        labeled_idxs, unlabeled_idxs = relabel_dataset(trainset, labels)
    assert len(trainset.imgs) == len(labeled_idxs)+len(unlabeled_idxs)
    if config.labeled_batch_size < config.batch_size:
        assert len(unlabeled_idxs)>0
        batchSampler = TwoStreamBatchSamplerFix if config.model=='tempens' else TwoStreamBatchSampler
        batch_sampler = batchSampler(
            unlabeled_idxs, labeled_idxs, config.batch_size, config.labeled_batch_size)
    else:
        sampler = SubsetRandomSampler(labeled_idxs)
        batch_sampler = BatchSampler(sampler, config.batch_size, drop_last=True)
    train_loader = torch.utils.data.DataLoader(trainset,
                                               batch_sampler=batch_sampler,
                                               num_workers=config.workers,
                                               pin_memory=True)

    evaldir = os.path.join(datadir, config.eval_subdir)
    evalset = torchvision.datasets.ImageFolder(evaldir,eval_transform)
    eval_loader = torch.utils.data.DataLoader(evalset,
                                              batch_size=config.batch_size,
                                              shuffle=False,
                                              num_workers=2*config.workers,
                                              pin_memory=True,
                                              drop_last=False)
    return train_loader, eval_loader, len(unlabeled_idxs)

def create_data_loaders_close(train_transform, 
                              eval_transform, 
                              datadir,
                              num_classes,
                              config):
    ## construct close-set dict
    rclose = random.sample(list(range(num_classes)), config.close) 
    close_set_dict = {rc:i for i, rc in enumerate(rclose)}
    ## set-up two stream dataloader
    if config.twice:
        train_transform = twice(train_transform)
        eval_transform = twice(eval_transform)

    traindir = os.path.join(datadir, config.train_subdir)
    trainset = torchvision.datasets.ImageFolder(traindir, train_transform)
    ## relabel train dataset
    if config.labels:
        with open(config.labels) as f:
            labels = dict(line.split(' ') for line in f.read().splitlines())
        labeled_idxs, unlabeled_idxs = relabel_dataset_close(trainset, labels, close_set_dict)
    #assert len(trainset.imgs) == len(labeled_idxs)+len(unlabeled_idxs)
    print("close-set map: ", close_set_dict)
    print("total training samples: ", len(trainset.imgs))
    print("labeled samples: ", len(labeled_idxs))
    print("unlabeled samples: ", len(unlabeled_idxs))
    if config.labeled_batch_size < config.batch_size:
        assert len(unlabeled_idxs)>0
        if config.model=='tempens':
            batchSampler = TwoStreamBatchSamplerFix
        else:
            batchSampler = TwoStreamBatchSampler
        batch_sampler = batchSampler(
            unlabeled_idxs, labeled_idxs, config.batch_size, config.labeled_batch_size)
    else:
        sampler = SubsetRandomSampler(labeled_idxs)
        batch_sampler = BatchSampler(sampler, config.batch_size, drop_last=True)
    train_loader = torch.utils.data.DataLoader(trainset,
                                               batch_sampler=batch_sampler,
                                               num_workers=config.workers,
                                               pin_memory=True)
    ## relabel test dataset
    evaldir = os.path.join(datadir, config.eval_subdir)
    evalset = torchvision.datasets.ImageFolder(evaldir,eval_transform)
    eval_idxs = relabel_test_dataset_close(evalset, close_set_dict)
    print("test samples: ", len(eval_idxs))
    eval_sampler = SubsetRandomSampler(eval_idxs)
    eval_batch_sampler = BatchSampler(eval_sampler, config.batch_size, drop_last=False)
    eval_loader  = torch.utils.data.DataLoader(evalset,
                                               batch_sampler=eval_batch_sampler,
                                               num_workers=2*config.workers,
                                               pin_memory=True)
    return train_loader, eval_loader, len(unlabeled_idxs)

def create_loss_fn(config):
    if config.loss == 'mse':
        criterion = nn.mseloss()
    elif config.loss == 'soft':
        # for pytorch 0.4.1 and 1.0.0
        criterion = nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduction='none')
        # for pytorch 0.4.0
        #criterion = nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduce=False)
    return criterion

def create_optim(params, config):
    if config.optim == 'sgd':
        optimizer = optim.SGD(params, config.lr,
                              momentum=config.momentum,
                              weight_decay=config.weight_decay,
                              nesterov=config.nesterov)
    elif config.optim == 'adam':
        optimizer = optim.Adam(params, config.lr)
    return optimizer

def create_lr_scheduler(optimizer, config):
    if config.lr_scheduler == 'cos':
        scheduler = lr_scheduler.CosineAnnealingLR(optimizer,
                                                   T_max=config.epochs,
                                                   eta_min=config.min_lr)
    elif config.lr_scheduler == 'multistep':
        if config.steps=="":
            return None
        scheduler = lr_scheduler.MultiStepLR(optimizer,
                                             milestones=config.steps,
                                             gamma=config.gamma)
    elif config.lr_scheduler == 'exp-warmup':
        lr_lambda = exp_warmup(config.rampup_length,
                               config.rampdown_length,
                               config.epochs)
        scheduler = lr_scheduler.LambdaLR(optimizer,
                                          lr_lambda=lr_lambda)
    elif config.lr_scheduler == 'none':
        scheduler = None
    return scheduler

def main(config):
    print("pytorch version : {}".format(torch.__version__))
    dataset_config = datasets.load_data[config.dataset]()
    train_loader, eval_loader, unlabeled_size = create_data_loaders(**dataset_config, config=config)
    #train_loader, eval_loader, unlabeled_size = create_data_loaders_close(**dataset_config, config=config)

    num_classes = dataset_config.pop('num_classes')
    net = arch[config.arch](num_classes)

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    criterion = create_loss_fn(config)
    net = net.to(device)
    optimizer = create_optim(net.parameters(), config)
    scheduler = create_lr_scheduler(optimizer, config)

    if config.model == 'ema':
        net2 = arch[config.arch](num_classes)
        net2 = net2.to(device)
        trainer = consTrainer.Trainer(net, net2, optimizer, criterion, device, config)
    if config.model == 'db_ema':
        net2 = arch[config.arch](num_classes)
        net2 = net2.to(device)
        trainer = consDBTrainer.Trainer(net, net2, optimizer, criterion, device, config)
    elif config.model == 'pi':
        trainer = piTrainer.Trainer(net, net, optimizer, criterion, device, config)
    elif config.model == 'tempens':
        trainer = tempensTrainer.Trainer(net, optimizer, criterion, device, unlabeled_size, num_classes, config)
    elif config.model == 'hybrid':
        net2 = arch[config.arch](num_classes)
        net2 = net2.to(device)
        trainer = hybridTrainer.Trainer(net, net2, optimizer, criterion, device, config)
    trainer.loop(config.epochs, train_loader, eval_loader, scheduler=scheduler)

if __name__ == '__main__':
    from util.Config import create_parser
    config = create_parser().parse_args()
    main(config)
