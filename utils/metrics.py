import torch
from pdb import set_trace

DEBUG = False

def evaluation(labels, preds, open_label):
    ##=== close set accuracy ===
    close_mask = labels.ne(open_label)
    num_close  = close_mask.float().sum()
    correct    = (preds[close_mask]==labels[close_mask]).float().sum()
    close_acc  = correct / num_close
    ##=== open-set f-measure ===
    tp, fp, fn, tn = 0., 0., 0., 0.
    for i in range(labels.size(0)):
        tp += 1 if (preds[i]==labels[i] and labels[i]!=open_label) else 0
        #fp += 1 if (preds[i]!=labels[i] and labels[i]!=open_label and preds[i]!=open_label) else 0
        fp += 1 if (preds[i]!=labels[i] and labels[i]!=open_label) else 0
        fn += 1 if (preds[i]!=labels[i] and labels[i]==open_label) else 0
        tn += 1 if (preds[i]==labels[i] and labels[i]==open_label) else 0
    precision = tp / (tp + fp +1e-12)
    recall    = tp / (tp + fn +1e-12)
    if DEBUG:
        print(f'n_close: {num_close}\t tp: {tp}\t fp: {fp}\t fn: {fn}\t tn: {tn}\t precision: {precision:.3f}\t recall: {recall:.3f}')
    f1_score = 2.0* ((precision * recall) / (precision + recall +1e-12))
    return close_acc, f1_score

def f1_score(labels, preds, open_label):
    ##=== open-set f-measure ===
    tp, fp, fn = 0., 0., 0.
    for i in range(labels.size(0)):
        tp += 1 if (preds[i]==labels[i] and labels[i]!=open_label) else 0
        fp += 1 if (preds[i]!=labels[i] and labels[i]!=open_label and preds[i]!=open_label) else 0
        fn += 1 if (preds[i]!=labels[i] and labels[i]==open_label) else 0
    precision = tp / (tp + fp +1e-12)
    recall    = tp / (tp + fn +1e-12)
    f1_score = 2.0* ((precision * recall) / (precision + recall +1e-12))
    return f1_score

def bi_evaluation(labels, preds, open_label=None):
    ## binarize the labels and preds
    blabels = labels.clone()
    bpreds = preds.clone()
    if open_label is not None:
        blabels[labels.ne(open_label)] = open_label+1
        bpreds[preds.ne(open_label)] = open_label+1
        if DEBUG:
            print("labels: ", blabels)
            print("labels: ", blabels.unique())
            print("preds: ",  bpreds)
            print("preds: ",  bpreds.unique())
    ##=== binary accuracy ===
    bi_acc = (blabels==bpreds).float().sum() / blabels.size(0)
    bi_err = (blabels!=bpreds).float().sum() / blabels.size(0)
    return bi_acc.item(), bi_err.item()

def area_under_roc(labels, score, open_label=None):
    from sklearn.metrics import roc_auc_score
    blabels = labels.clone().cpu().numpy()  # int64
    bscore = score.clone().cpu().numpy()
    if open_label is not None:
        open_mask =  (blabels==open_label)
        close_mask = (blabels!=open_label)
        blabels[open_mask] = -1
        blabels[close_mask] = 1
    print(set(blabels))
    return roc_auc_score(blabels, bscore)

def roc(labels, score, open_label=None):
    from sklearn.metrics import roc_curve
    blabels = labels.clone().cpu().numpy()  # int64
    bscore = score.clone().cpu().numpy()
    if open_label is not None:
        open_mask  = (blabels==open_label)
        close_mask = (blabels!=open_label)
        blabels[open_mask] = -1
        blabels[close_mask] = 1
    print(set(blabels))
    return roc_curve(blabels, bscore, pos_label=1)

def select_theta(score, prior):
    size = score.size(0)
    score_sorted = score.sort()[0]
    pos = max(0,int(size*(1. - prior) -1))
    return score[pos]
