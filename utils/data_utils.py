import os
import itertools
import numpy as np
from torch.utils.data.sampler import Sampler

NO_LABEL = -1

class TransformTwice:

    def __init__(self, transform):
        self.transform = transform

    def __call__(self, inp):
        out1 = self.transform(inp)
        out2 = self.transform(inp)
        return out1, out2

def relabel_dataset(dataset, labels):
    unlabeled_idxs = []
    for idx in range(len(dataset.imgs)):
        path, lable_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            dataset.imgs[idx] = path, label_idx
            del labels[filename]
        else:
            dataset.imgs[idx] = path, NO_LABEL
            unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    labeled_idxs = sorted(set(range(len(dataset.imgs)))-set(unlabeled_idxs))
    return labeled_idxs, unlabeled_idxs


def relabel_dataset_open(dataset, labels, close_set_dict):
    unlabeled_idxs = []
    labeled_open_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            if label_idx in close_set_dict:
                dataset.imgs[idx] = path, close_set_dict[label_idx]
            else:
                dataset.imgs[idx] = path, NO_LABEL
                labeled_open_idxs.append(idx)
            del labels[filename]
        else:
            dataset.imgs[idx] = path, NO_LABEL
            unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    labeled_idxs = set(range(len(dataset.imgs)))-set(unlabeled_idxs)
    labeled_idxs = sorted(labeled_idxs - set(labeled_open_idxs))
    return labeled_idxs, unlabeled_idxs

def relabel_dataset_open_visual(dataset, labels, close_set_dict):
    unlabeled_idxs = []
    labeled_open_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            if label_idx in close_set_dict:
                dataset.imgs[idx] = path, close_set_dict[label_idx]
            else:
                dataset.imgs[idx] = path, NO_LABEL
                labeled_open_idxs.append(idx)
            del labels[filename]
        else:
            dataset.imgs[idx] = path, NO_LABEL*(label_idx+1)
            unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    labeled_idxs = set(range(len(dataset.imgs)))-set(unlabeled_idxs)
    labeled_idxs = sorted(labeled_idxs - set(labeled_open_idxs))
    return labeled_idxs, unlabeled_idxs


def relabel_dataset_open_plus(dataset, labels, close_set_dict, unlabeled_set):
    unlabeled_idxs = []
    labeled_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            if label_idx in close_set_dict:
                dataset.imgs[idx] = path, close_set_dict[label_idx]
                labeled_idxs.append(idx)
            else:
                dataset.imgs[idx] = path, NO_LABEL
            del labels[filename]
        elif label_idx in unlabeled_set:
            dataset.imgs[idx] = path, NO_LABEL
            unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    return labeled_idxs, unlabeled_idxs

def relabel_dataset_open_plus_visual(dataset, labels, close_set_dict, unlabeled_set):
    unlabeled_idxs = []
    labeled_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            if label_idx in close_set_dict:
                dataset.imgs[idx] = path, close_set_dict[label_idx]
                labeled_idxs.append(idx)
            else:
                dataset.imgs[idx] = path, NO_LABEL
            del labels[filename]
        elif label_idx in unlabeled_set:
            dataset.imgs[idx] = path, NO_LABEL*(label_idx+1)
            unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    return labeled_idxs, unlabeled_idxs


def relabel_dataset_close(dataset, labels, close_set_dict):
    unlabeled_idxs = []
    labeled_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            if label_idx in close_set_dict:
                dataset.imgs[idx] = path, close_set_dict[label_idx]
                labeled_idxs.append(idx)
            else:
                dataset.imgs[idx] = path, NO_LABEL
            del labels[filename]
        else:
            dataset.imgs[idx] = path, NO_LABEL
            if label_idx in close_set_dict:
                unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    return labeled_idxs, unlabeled_idxs


def relabel_test_dataset_open(dataset, close_set_dict):
    open_label = len(close_set_dict)
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]

        if label_idx in close_set_dict:
            dataset.imgs[idx] = path, close_set_dict[label_idx]
        else:
            dataset.imgs[idx] = path, open_label
    return dataset

def relabel_test_dataset_open_plus(dataset, close_set_dict, unlabeled_set):
    open_label = len(close_set_dict)
    test_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]

        if label_idx in unlabeled_set:
            if label_idx in close_set_dict:
                dataset.imgs[idx] = path, close_set_dict[label_idx]
            else:
                dataset.imgs[idx] = path, open_label
            test_idxs.append(idx)
    return test_idxs


def relabel_test_dataset_close(dataset, close_set_dict):
    open_label = len(close_set_dict)
    labeled_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]

        if label_idx in close_set_dict:
            dataset.imgs[idx] = path, close_set_dict[label_idx]
            labeled_idxs.append(idx)
        else:
            dataset.imgs[idx] = path, open_label
    return labeled_idxs


def relabel_dataset_visual(dataset, labels):
    """make the labels of unlabeled data visual
    """
    unlabeled_idxs = []
    for idx in range(len(dataset.imgs)):
        path, label_idx = dataset.imgs[idx]
        filename = os.path.basename(path)
        if filename in labels:
            label_idx = dataset.class_to_idx[labels[filename]]
            dataset.imgs[idx] = path, label_idx
            del labels[filename]
        else:
            dataset.imgs[idx] = path, NO_LABEL*(label_idx+1)
            unlabeled_idxs.append(idx)

    if len(labels)!=0:
        message = "List of unlabeled contains {} unknow files: {}, ..."
        some_missing = ', '.join(list(labels.keys())[:5])
        raise LookupError(message.format(len(labels), some_missing))

    labeled_idxs = sorted(set(range(len(dataset.imgs)))-set(unlabeled_idxs))
    return labeled_idxs, unlabeled_idxs


class TwoStreamBatchSampler(Sampler):
    """Iterate two sets of indices
    """
    def __init__(self, primary_indices, secondary_indices, batch_size, secondary_batch_size):
        self.primary_indices = primary_indices
        self.primary_batch_size = batch_size - secondary_batch_size
        self.secondary_indices = secondary_indices
        self.secondary_batch_size = secondary_batch_size

        assert len(self.primary_indices) >= self.primary_batch_size > 0
        assert len(self.secondary_indices) >= self.secondary_batch_size > 0

    def __iter__(self):
        primary_iter = iterate_once(self.primary_indices)
        secondary_iter = iterate_eternally(self.secondary_indices)
        return (
            secondary_batch + primary_batch
            for (primary_batch, secondary_batch)
            in zip(grouper(primary_iter, self.primary_batch_size),
                   grouper(secondary_iter, self.secondary_batch_size))
        )

    def __len__(self):
        return len(self.primary_indices) // self.primary_batch_size

class TwoStreamBatchSamplerFix(Sampler):
    """Iterate two sets of indices
    But fix the order of unlabeled data set
    """
    def __init__(self, primary_indices, secondary_indices, batch_size, secondary_batch_size, is_shuffle=True):
        self.is_shuffle = is_shuffle
        self.primary_indices = iterate_once(primary_indices) if is_shuffle else primary_indices
        self.primary_batch_size = batch_size - secondary_batch_size
        self.secondary_indices = secondary_indices
        self.secondary_batch_size = secondary_batch_size

        assert len(self.primary_indices) >= self.primary_batch_size > 0
        assert len(self.secondary_indices) >= self.secondary_batch_size > 0

    def __iter__(self):
        primary_iter = self.primary_indices
        secondary_iter = iterate_eternally(self.secondary_indices, self.is_shuffle)
        return (
            secondary_batch + primary_batch
            for (primary_batch, secondary_batch)
            in zip(grouper(primary_iter, self.primary_batch_size),
                   grouper(secondary_iter, self.secondary_batch_size))
        )

    def __len__(self):
        return len(self.primary_indices) // self.primary_batch_size

def iterate_once(iterable):
    return np.random.permutation(iterable)

def iterate_eternally(indices, is_shuffle=True):
    shuffleFunc = np.random.permutation if is_shuffle else lambda x: x
    def infinite_shuffles():
        while True:
            yield shuffleFunc(indices)
    return itertools.chain.from_iterable(infinite_shuffles())

def grouper(iterable, n):
    args = [iter(iterable)]*n
    return zip(*args)
