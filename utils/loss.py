#!coding:utf-8
import torch
from torch.nn import functional as F

from utils.data_utils import NO_LABEL

from pdb import set_trace
import numpy as np

def kl_div_with_logit(input_logits, target_logits):
    assert input_logits.size()==target_logits.size()
    logits = F.softmax(input_logits, dim=1)
    qlogq = torch.sum(logits* F.log_softmax(input_logits, dim=1), dim=1)
    qlogp = torch.sum(logits* F.log_softmax(target_logits, dim=1), dim=1)
    return torch.mean(qlogq - qlogp)

def entropy_y_x(logit):
    p = F.softmax(logit, dim=1)
    return -1.0* torch.mean(torch.sum(p* F.log_softmax(logit,dim=1), dim=1))

def softmax_loss(input_logits, target_logits):
    assert input_logits.size()==target_logits.size()
    target_soft = F.softmax(target_logits, dim=1)
    return -1.0* torch.sum(target_soft* F.log_softmax(input_logits,dim=1), dim=1)

def sym_mse(logit1, logit2):
    assert logit1.size()==logit2.size()
    return torch.mean((logit1 - logit2)**2)

def sym_mse_with_softmax(logit1, logit2):
    assert logit1.size()==logit2.size()
    return torch.mean((F.softmax(logit1,1) - F.softmax(logit2,1))**2)

def mse_with_softmax(logit1, logit2):
    assert logit1.size()==logit2.size()
    return F.mse_loss(F.softmax(logit1,1), F.softmax(logit2,1))

def sntg_loss(feats, targets, logits, m=1.):
    ##=== construct sntg logit ===
    target_idxs = targets.eq(NO_LABEL)
    sntg_logits = targets.clone()
    sntg_logits[target_idxs] = logits[target_idxs]
    ## random shuffle?
    minibatch_size = targets.size(0)
    ridxs = torch.randperm(minibatch_size)
    if minibatch_size%2!=0:
        minibatch_size -= 1            # size - 1
        ridxs = ridxs[:minibatch_size] # drop the last
    feats, sntg_logits = feats[ridxs], sntg_logits[ridxs]
    ##=== partition ===
    sntg_logits1 = sntg_logits[:minibatch_size//2]
    sntg_logits2 = sntg_logits[minibatch_size//2:]
    feats1 = feats[:minibatch_size//2]
    feats2 = feats[minibatch_size//2:]
    maskn = (sntg_logits1==sntg_logits2).float()
    ##=== positive loss ===
    diff2 = torch.sum((feats1 - feats2)**2,1)
    pos = diff2 *maskn 
    ##=== negitive loss ===
    neg = m - (1.-maskn)*diff2**(.5)
    neg = torch.clamp(neg, min=0)**2
    return torch.sum(pos + neg) 
    #return torch.mean(pos + neg) 


def pu_loss(x_in, y, prior, loss_func=(lambda x: torch.sigmoid(-x)),
            gamma=1,
            beta=0,
            nnpu=True):
    positive  = y.eq(1).float().unsqueeze(1)
    unlabeled = y.eq(-1).float().unsqueeze(1) 
    n_positive, n_unlabeled = positive.sum()+1e-9, unlabeled.sum()+1e-9
    y_positive  = loss_func(x_in)
    y_unlabeled = loss_func(-x_in)
    positive_risk = torch.sum(prior * positive / n_positive * y_positive)
    negative_risk = torch.sum((unlabeled/n_unlabeled - prior* positive/n_positive) * y_unlabeled)
    objective = positive_risk + negative_risk
    if negative_risk.data < -beta:
        x_out = -gamma*negative_risk
    else:
        x_out = objective
    #set_trace()
    if(np.isnan(x_out.item())):
        set_trace()
    return x_out

def weighted_sntg_loss(feats, targets, logits, probs, m=1.):
    ##=== construct sntg logit ===
    assert logits.size()==probs.size()
    target_idxs = targets.eq(NO_LABEL)
    sntg_logits = targets.clone()
    sntg_logits[target_idxs] = logits[target_idxs]
    sntg_probs = targets.clone().float()
    sntg_probs[target_idxs] = probs[target_idxs]
    ## random shuffle?
    minibatch_size = targets.size(0)
    ridxs = torch.randperm(minibatch_size)
    if minibatch_size%2!=0:
        minibatch_size -= 1            # size - 1
        ridxs = ridxs[:minibatch_size] # drop the last
    feats, sntg_logits = feats[ridxs], sntg_logits[ridxs]
    sntg_probs = sntg_probs[ridxs]
    ##=== partition ===
    sntg_logits1 = sntg_logits[:minibatch_size//2]
    sntg_logits2 = sntg_logits[minibatch_size//2:]
    feats1 = feats[:minibatch_size//2]
    feats2 = feats[minibatch_size//2:]
    maskn = (sntg_logits1==sntg_logits2).float()
    ##=== positive loss ===
    diff2 = torch.sum((feats1 - feats2)**2,1)
    pos = diff2 *maskn 
    ##=== negitive loss ===
    neg = m - (1.-maskn)*diff2**(.5)
    neg = torch.clamp(neg, min=0)**2
    ##=== weights ===
    sntg_probs1 = sntg_probs[:minibatch_size//2]
    sntg_probs2 = sntg_probs[minibatch_size//2:]
    weights = sntg_probs1 * sntg_probs2

    return torch.sum(weights * (pos + neg))
    #return torch.mean(weights * (pos + neg))


