import torchvision.transforms as transforms
from utils.augmentation import RandomTranslateWithReflect

load_data = {}
def load(dataset_name):
    def warpper(f):
        load_data[dataset_name] = f
        return f
    return warpper

@load('cifar10')
def cifar10():
    channel_stats = dict(mean=[0.4914, 0.4822, 0.4465],
                         std=[0.2470,  0.2435,  0.2616])
    train_transform = transforms.Compose([
        RandomTranslateWithReflect(4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/cifar/cifar10/by-image',
        'num_classes': 10
    }

@load('cifar10_plus')
def cifar10_plus():
    channel_stats = dict(mean=[0.4914, 0.4822, 0.4465],
                         std=[0.2470,  0.2435,  0.2616])
    train_transform = transforms.Compose([
        RandomTranslateWithReflect(4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/cifar/cifar10_plus/by-image',
        'num_classes': 10
    }


@load('cifar100')
def cifar100():
    channel_stats = dict(mean=[0.4914, 0.4822, 0.4465],
                         std=[0.2470,  0.2435,  0.2616])
    train_transform = transforms.Compose([
        RandomTranslateWithReflect(4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/cifar/cifar100/by-image',
        'num_classes': 100
    }


@load('svhn')
def svhn():
    channel_stats = dict(mean=[0.4377, 0.4437, 0.4727],
                         std=[0.1976,  0.2003,  0.1992])
    train_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/svhn/by-image',
        'num_classes': 10
    }

@load('fashion')
def fashion():
    channel_stats = dict(mean=[0.2885, 0.2885, 0.2885],
                         std=[0.2557,  0.2557,  0.2557])
    train_transform = transforms.Compose([
        #RandomTranslateWithReflect(4),
        #transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/fashion/by-image',
        'num_classes': 10
    }

@load('mnist')
def mnist():
    channel_stats = dict(mean=[0.1318, 0.1318, 0.1318],
                         std=[0.1930,  0.1930,  0.1930])
    train_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/mnist/by-image',
        'num_classes': 10
    }

@load('cinic10')
def cinic10():
    channel_stats = dict(mean=[0.47889522, 0.47227842, 0.43047404],
                         std=[0.24205776,  0.23828046,  0.25874835])
    train_transform = transforms.Compose([
        RandomTranslateWithReflect(4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/cinic10/by-image',
        'num_classes': 10
    }

@load('tiny-imagenet')
def tiny_imagenet():
    channel_stats = dict(mean=[0.4914, 0.4822, 0.4465],
                         std=[0.2470,  0.2435,  0.2616])
    train_transform = transforms.Compose([
        RandomTranslateWithReflect(4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])
    eval_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(**channel_stats)
    ])

    return {
        'train_transform': train_transform,
        'eval_transform': eval_transform,
        'datadir': './data-local/images/tiny-imagenet/by-image',
        'num_classes': 10
    }


