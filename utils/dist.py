import torch
import torch.nn.functional as F

DEBUG = False

def eucl_dist(x, y):
    """ Compute Pairwise (Squared Euclidean) Distance

    Input:
        x: embedding of size M x D
        y: embedding of size N x D

    Output:
        dist: pairwise distance of size M x N
    """
    x2 = torch.sum(x**2, dim=1, keepdim=True).expand(-1, y.size(0))
    y2 = torch.sum(y**2, dim=1, keepdim=True).t().expand(x.size(0), -1)
    xy = x.mm(y.t())
    return x2 - 2*xy + y2

def rbf_graph(x, y, sigma):
    diff = eucl_dist(x, y)
    g = torch.exp(diff / (-2.0 * sigma**2))
    return g / torch.sum(g, dim=1, keepdim=True)

def knn_graph(x, y, k):
    diff = eucl_dist(x, y)
    val, idx = diff.topk(k, dim=1, largest=False)
    g = torch.zeros(x.size(0), x.size(0))
    g.scatter_(1, idx.cpu(), 1)
    #return g / torch.sum(g, dim=1, keepdim=True)
    return normalize_adj(g), idx

def normalize_adj(adj):
    # D = sum{A_i}
    rowsum = torch.sum(adj, dim=1)
    # D^(-0.5)
    d_inv_sqrt = rowsum**(-0.5)
    d_inv_sqrt[torch.isnan(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = torch.diag(d_inv_sqrt)
    # D^(-0.5) * A * D^(-0.5)
    return d_mat_inv_sqrt.mm(adj).mm(d_mat_inv_sqrt)

def knn_rbf_graph(x, y, k, sigma=1.0):
    diff = eucl_dist(x, y)
    val, idx = diff.topk(k, dim=1, largest=False)
    val = torch.exp(val / (-2.0 * sigma**2))
    g = torch.zeros(x.size(0), x.size(0))
    g.scatter_(1, idx.cpu(), val.cpu())
    return g / torch.sum(g, dim=1, keepdim=True), idx

def knn_cos_graph(x, y, k):
    diff = cosine_dist(x, y)
    val, idx = diff.topk(k, dim=1)
    g = torch.zeros(x.size(0), x.size(0))
    g.scatter_(1, idx.cpu(), val.cpu())
    return g / torch.sum(g, dim=1, keepdim=True), idx

def knn_atten_graph(x, y, k):
    diff = eucl_dist(x, y)
    val, idx = diff.topk(k, dim=1, largest=False)
    xy = x.mm(y.t())
    g = torch.zeros(x.size(0), x.size(0))
    g.scatter_(1, idx.cpu(), xy.gather(1, idx).cpu())
    g = g + g.t()
    return g / torch.sum(g, dim=1, keepdim=True), idx

def cos_knn_cos_graph_sparse(x, y, k):
    diff = cosine_dist(x, y)
    val, vidx = diff.topk(k, dim=1)
    ## normalization
    val /= torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    return g, vidx 


def eucl_knn_graph_sparse(x, y, k):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    ## normalization
    val = 1.0/val.size(1)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    return g, vidx 

def eucl_knn_rbf_graph_sparse(x, y, k, sigma=1.0):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    val = torch.exp(val / (-2.0* sigma**2))
    ## normalization
    val /= torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    return g, vidx 

def eucl_knn_rbf_graph_sparse2(x, y, k, sigma=1.0):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    val = torch.exp(val / (-2.0* sigma**2))
    ## normalization
    val /= torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    vval = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, vval, torch.Size([n,n]))
    return g, vidx, val 


def eucl_knn_rbf_graph_sparse_theshold(x, y, k, sigma=1.0):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    if DEBUG:
        print("check knn val threshold")
        print(val.size())
        print(val[:10])
    val = torch.exp(val / (-2.0* sigma**2))
    ## normalization
    val /= torch.sum(val, dim=1, keepdim=True)
    ## filtering
    th = torch.mean(val[:,1:])
    val[val.le(th)] = 0
    ## normalization
    val /= torch.sum(val, dim=1, keepdim=True)
    if DEBUG: 
        print("check normalized val threshold")
        print("normalized val again")
        print(val[:10])
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    return g, vidx 

def eucl_knn_atten_graph_sparse_theshold(x, y, k):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    if DEBUG:
        print("check knn val threshold")
        print(val.size())
        print(val[:10])
    xy = x.mm(y.t())
    val = xy.gather(1, vidx).cpu()
    ## normalization
    val /= 2.0*torch.sum(val, dim=1, keepdim=True)
    ## filtering
    th = torch.mean(val[:,1:])
    val[val.le(th)] = 0
    ## normalization
    val /= 2.0*torch.sum(val, dim=1, keepdim=True)
    if DEBUG:
        print("check normalized val threshold")
        print("normalized val again")
        print(val[:10])
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    g = g + g.t()
    return g, vidx 


def eucl_knn_atten_graph_sparse(x, y, k):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    xy = x.mm(y.t())
    val = xy.gather(1, vidx).cpu()
    ## normalization
    val /= 2.0*torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    g = g + g.t()
    return g, vidx 

def eucl_knn_atten_graph_sparse_pos(x, y, k):
    diff = eucl_dist(x, y)
    val, vidx = diff.topk(k, dim=1, largest=False)
    xy = x.mm(y.t())
    xy.clamp_(min=0) # only need positive
    val = xy.gather(1, vidx).cpu()
    #val.clamp_(min=0) # only need positive
    ## normalization
    val /= 2.0*torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    g = g + g.t()
    return g, vidx 

def atten_knn_atten_graph_sparse(x, y, k):
    """LP for Deep SSL"""
    xy = x.mm(y.t())
    val, vidx = xy.topk(k, dim=1)
    ## normalization
    val /= 2.0*torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    g = g + g.t()
    return g, vidx

def atten2_knn_atten2_graph_sparse(x, y, k):
    """GCN Label Noise Cleaner"""
    xy = x.mm(y.t())
    xy = torch.exp( xy - xy.max(dim=1, keepdim=True)[0] )
    val, vidx = xy.topk(k, dim=1)
    ## normalization
    val /= torch.sum(val, dim=1, keepdim=True)
    ## sparse
    n = vidx.size(0)
    aj = torch.arange(0, end=n)
    aj = aj.unsqueeze(1).expand(-1,k).contiguous().view(-1)
    idx = vidx.contiguous().view(-1)
    idx = torch.stack((aj,idx))
    val = val.contiguous().view(-1)
    g = torch.sparse.FloatTensor(idx, val, torch.Size([n,n]))
    return g, vidx


def cosine_dist(x, y):
    """ Compute consin dist

    Input:
        x: embedding of size M x D
        y: embedding of size N x D

    Output:
        dist: pairwise distance of size M x N
    """
    xy = x.mm(y.t())
    x_norm = torch.norm(x, p=2, dim=1, keepdim=True)
    y_norm = torch.norm(y, p=2, dim=1, keepdim=True)
    xy_norm = x_norm.mm( y_norm.t() )
    return xy / xy_norm.add(1e-10)


def neighbor_graph(x):
    neighbor_n = x.size(0)
    x1 = x.unsqueeze(0).expand(neighbor_n, -1)
    x2 = x.unsqueeze(1).expand(-1, neighbor_n)
    return x1.eq(x2).float()
