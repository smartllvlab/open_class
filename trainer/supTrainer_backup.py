#!coding:utf-8
import torch
from torch.nn import functional as F
import time, datetime

from pathlib import Path
from util.ramps import exp_rampup

class Trainer:

    def __init__(self, model, optimizer, loss_fn, device, config,
                 writer=None, save_freq=5):
        self.model = model
        self.model_type = config.model
        self.optimizer = optimizer
        self.loss_fn = loss_fn
        self.save_dir = '{}{}_{}_{}_{}'.format(config.save_dir,
                                               config.arch,
                                               config.dataset,
                                               config.model,
                                               datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_freq = config.save_freq
        self.device = device
        self.writer = writer
        self.global_step = 0
        self.epoch = 0
        self.rampup = exp_rampup(config.weight_rampup)


    def _iteration(self, data_loader, print_freq, is_train=True):
        loop_acc, loop_loss = [], []
        labeled_n = 0
        mode = "train" if is_train else "test"
        for batch_idx, (data, targets) in enumerate(data_loader):
            self.global_step += 1
            if isinstance(data, list):
                data = data[0]
            data, targets = data.to(self.device), targets.to(self.device)
            outputs, _ = self.model(data)
            loss = torch.mean(self.loss_fn(outputs, targets))
            if is_train:
                ##--- backwark
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            labeled_n += data.size(0)

            loop_loss.append(loss.item())
            acc = targets.eq(outputs.max(1)[1]).float().sum().item()
            loop_acc.append(acc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[{mode}][{batch_idx:<3}]\t"\
                    f"loss: {loss.item()/data.size(0):.6f}\t Acc: {acc/data.size(0):.6%}")
            if self.writer:
                self.writer.add_scalar(mode+'_global_loss', loss.item()/data.size(0), self.global_step)
                self.writer.add_scalar(mode+'_global_accuracy', acc/data.size(0), self.global_step)
        print(f">>>[{mode}]loss: {sum(loop_loss)/labeled_n:.8f}\t "\
            f"Acc: {sum(loop_acc)/labeled_n:.8%}")
        if self.writer:
            self.writer.add_scalar(mode+'_epoch_loss', sum(loop_loss)/labeled_n, self.epoch)
            self.writer.add_scalar(mode+'_epoch_accuracy', sum(loop_acc)/labeled_n, self.epoch)

        return loop_loss, loop_acc

    def train(self, data_loader, print_freq=20):
        self.model.train()
        with torch.enable_grad():
            loss, correct = self._iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        with torch.no_grad():
            loss, correct = self._iteration(data_loader, print_freq, is_train=False)

    def loop(self, epochs, train_data, test_data, scheduler=None, print_freq=-1):
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None:
                scheduler.step()
                #print(">>>lr factor: {}".format(scheduler.lr_lambdas[0](ep)))
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, print_freq)
            print("------ Testing epochs: {} ------".format(ep))
            self.test(test_data, print_freq)
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
