#!coding:utf-8
import torch
from torch import nn
from torch.nn import functional as F
import time, datetime

from utils.dist import *
from pathlib import Path
from utils.data_utils import NO_LABEL
from utils.ramps import exp_rampup
from utils.loss import kl_div_with_logit, mse_with_softmax

import numpy as np

class Trainer:

    def __init__(self, model, pi_model, optimizer, loss_fn, device, config):
        self.model = model
        self.pi_model = pi_model
        self.optimizer = optimizer
        self.ce_loss = nn.CrossEntropyLoss(ignore_index=NO_LABEL)
        self.save_dir = '{}{}_{}_{}_{}'.format(config.save_dir,
                                               config.arch,
                                               config.dataset,
                                               config.model,
                                               datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_freq = config.save_freq
        self.print_freq = config.print_freq
        self.device = device
        self.labeled_bs = config.labeled_batch_size
        self.epoch = 0
        self.cons_weight = config.cons_weight
        self.rampup = exp_rampup(config.weight_rampup)
        self.cons_loss = mse_with_softmax

    def train_iteration(self, data_loader, print_freq):
        loop_loss = []
        accuracy = []
        labeled_n, lbs = 0, self.labeled_bs
        for batch_idx, (data, targets) in enumerate(data_loader):
            assert len(data)==2
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            targets = targets.to(self.device)
            ##=== class loss ===
            outputs, feats = self.model(data)
            labeled_loss = self.ce_loss(outputs, targets)
            ##=== consistency loss ===
            with torch.no_grad():
                pi_outputs, pi_feats = self.pi_model(data1)
            cons_loss = self.cons_loss(outputs, pi_outputs)
            cons_loss *= self.rampup(self.epoch)*self.cons_weight
            ##=== backwark ===
            loss = labeled_loss + cons_loss
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            ##=== log info ===
            labeled_n += lbs
            loop_loss.append(loss.item() / len(data_loader))
            acc     = targets.eq(outputs.max(1)[1]).sum().item()
            accuracy.append(acc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[train][{batch_idx:<3}]\t labeled: {labeled_loss.item():.3f}\t "\
                    f"cons: {cons_loss.item():.3f} \t "\
                    f"loss: {loss.item():.3f}\t Acc: {acc/lbs:.3%}")
        print(f">>>[train]loss: {sum(loop_loss):.3f}\t "\
            f"Acc: {sum(accuracy)/labeled_n:.3%}")

        return loop_loss, accuracy

    def test_iteration(self, data_loader, print_freq):
        loop_loss = []
        accuracy = []
        labeled_n = 0
        for batch_idx, (data, targets) in enumerate(data_loader):
            assert len(data)==2
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            targets = targets.to(self.device)
            ##=== class loss ===
            outputs, feats = self.model(data)
            labeled_loss = self.ce_loss(outputs, targets)
            ##=== consistency loss ===
            with torch.no_grad():
                pi_outputs, pi_feats = self.pi_model(data1)
            cons_loss = self.cons_loss(outputs, pi_outputs)
            cons_loss *= self.rampup(self.epoch)*self.cons_weight
            ##=== loss ===
            loss = labeled_loss + cons_loss
            ##=== log info ===
            lbs = data.size(0)
            labeled_n += lbs
            loop_loss.append(loss.item() / len(data_loader))
            acc     = targets.eq(outputs.max(1)[1]).sum().item()
            accuracy.append(acc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[test][{batch_idx:<3}]\t labeled: {labeled_loss.item():.3f}\t "\
                    f"cons: {cons_loss.item():.3f} \t "\
                    f"loss: {loss.item():.3f}\t Acc: {acc/lbs:.3%}")
        print(f">>>[test]loss: {sum(loop_loss):.3f}\t "\
            f"Acc: {sum(accuracy)/labeled_n:.3%}")

        return loop_loss, accuracy

    def obtain_prediction(self, data_loader):
        self.model.eval()
        self.pi_model.eval()
        preds, probs, labels = [], [], []
        for batch_idx, (data, targets) in enumerate(data_loader):
            assert len(data)==2
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            ##=== forward ===
            with torch.no_grad():
                outputs, _ = self.model(data)
            prob, pred = F.softmax(outputs.detach().cpu(),1).max(1)
            ##===  ===
            labels.append(targets)
            preds.append(pred)
            probs.append(prob)
        labels = torch.cat(labels)
        preds  = torch.cat(preds)
        probs  = torch.cat(probs)
        return labels, preds, probs


    def train(self, data_loader, print_freq=20):
        self.model.train()
        self.pi_model.train()
        with torch.enable_grad():
            loss, correct = self.train_iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        self.pi_model.eval()
        with torch.no_grad():
            loss, correct = self.test_iteration(data_loader, print_freq)

    def loop(self, epochs, train_data, test_data, scheduler=None):
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None:
                scheduler.step()
                #print(">>>lr factor: {}".format(scheduler.lr_lambdas[0](ep)))
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, self.print_freq)
            print("------ Testing epochs: {} ------".format(ep))
            self.test(test_data, self.print_freq)
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
