#!coding:utf-8
from pathlib import Path
import torch
from torch.nn import functional as F
from util.dist import eucl_dist

from util.datasets import NO_LABEL
import time, datetime
from util.ramps import exp_rampup
#from util.loss import mse_with_softmax

class Trainer:

    def __init__(self, model, optimizer, loss_fn, device, unlabeled_size, nClass, config, writer=None):
        self.model = model
        self.optimizer = optimizer
        self.loss_fn = loss_fn
        self.save_dir = '{}{}_{}_{}'.format(config.save_dir,
                                            config.arch,
                                            config.dataset,
                                            datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_freq = config.save_freq
        self.device = device
        self.writer = writer
        self.bs = config.batch_size
        self.labeled_bs = config.labeled_batch_size
        self.global_step = 0
        self.epoch = 0
        self.start_epoch = 0
        self.weight = config.cons_weight
        self.ema_decay = config.ema_decay
        self.nClass = nClass
        ## pseudo_label   : soft-prediction
        if config.labeled_batch_size < config.batch_size:
            self.temporal_predictions = torch.zeros(unlabeled_size // (self.bs-self.labeled_bs) * self.bs, nClass).to(self.device)
            self.epoch_predictions = torch.zeros(unlabeled_size // (self.bs-self.labeled_bs) * self.bs, nClass).to(self.device)
        else:
            self.temporal_predictions = None
        self.rampup = exp_rampup(config.rampup_length)
        
    def _iteration(self, data_loader, print_freq, is_train=True):
        loop_loss = []
        accuracy = []
        labeled_n = 0
        mode = "train" if is_train else "test"
        for batch_idx, (data, targets) in enumerate(data_loader):
            self.global_step += 1
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            targets = targets.to(self.device)
            outputs, feats = self.model(data)
            if is_train:
                labeled_bs = self.labeled_bs
                #targets[targets.le(NO_LABEL)] = NO_LABEL #remove the labels of unlabeled data
                labeled_loss = torch.sum(self.loss_fn(outputs, targets)) / labeled_bs
                with torch.no_grad():
                    indces = slice(batch_idx*self.bs, batch_idx*self.bs+self.bs, 1)
                    pseudo_labels = self.update_ema(outputs.clone().detach(), indces)
                    #pseudo_labels = self.epoch_predictions[indces].clone()
                    #self.epoch_predictions[indces] = outputs.clone().detach()
                unsup_loss = F.mse_loss(outputs, pseudo_labels)
                #unsup_loss = F.mse_loss(F.softmax(outputs,1), F.softmax(pseudo_labels,1))
                loss = labeled_loss + self.guassian_weight()*unsup_loss
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            else:
                labeled_bs = data.size(0)
                labeled_loss = unsup_loss = torch.Tensor([0])
                loss = torch.mean(self.loss_fn(outputs, targets))
            labeled_n += labeled_bs

            loop_loss.append(loss.item() / len(data_loader))
            acc = targets.eq(outputs.max(1)[1]).sum().item()
            accuracy.append(acc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[{mode}]loss[{batch_idx:<3}]\t labeled loss: {labeled_loss.item():.3f}\t"\
                      f"unsup loss: {unsup_loss.item():.3f}\t loss: {loss.item():.3f}\t Acc: {acc/labeled_bs:.3%}")
            if self.writer:
                self.writer.add_scalar(mode+'_global_loss', loss.item(), self.global_step)
                self.writer.add_scalar(mode+'_global_accuracy', acc/labeled_bs, self.global_step)
        print(f">>>[{mode}]loss\t loss: {sum(loop_loss):.3f}\t Acc: {sum(accuracy)/labeled_n:.3%}")
        if self.writer:
            self.writer.add_scalar(mode+'_epoch_loss', sum(loop_loss), self.epoch)
            self.writer.add_scalar(mode+'_epoch_accuracy', sum(accuracy)/labeled_n, self.epoch)

        return loop_loss, accuracy
    
    def guassian_weight(self):
        return self.weight * self.rampup(self.epoch)


    def train(self, data_loader, print_freq=20):
        self.model.train()
        with torch.enable_grad():
            loss, correct = self._iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        with torch.no_grad():
            loss, correct = self._iteration(data_loader, print_freq, is_train=False)

    def update_ema(self, predictions, indces):
        """update every iteration"""
        predictions = (self.ema_decay*self.temporal_predictions[indces]) + (1.0-self.ema_decay)*predictions
        self.temporal_predictions[indces,:] = predictions
        return predictions / (1.0 - self.ema_decay**((self.epoch-self.start_epoch)+1.0))

    def update_ema_predictions(self):
        """update every epoch"""
        self.temporal_predictions = (self.ema_decay*self.temporal_predictions) + (1.0-self.ema_decay)*self.epoch_predictions
        self.epoch_predictions = self.temporal_predictions / (1.0 - self.ema_decay**((self.epoch-self.start_epoch)+1.0))
        print(F.softmax(self.temporal_predictions[:3], 1))
        print(F.softmax(self.epoch_predictions[:3], 1))

    def loop(self, epochs, train_data, test_data, 
             scheduler=None, print_freq=-1):
        update_count = 0
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None:
                scheduler.step()
                #print(">>>lr factor: {}".format(scheduler.lr_lambdas[0](ep)))
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, print_freq)
            # temporal ensemble
            #self.update_ema_predictions()
            print("------ Testing epochs: {} ------".format(ep))
            self.test(test_data, print_freq)
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
