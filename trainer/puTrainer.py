#!coding:utf-8
import torch
from torch.nn import functional as F

import time, datetime
from pathlib import Path
from collections import defaultdict

from utils.data_utils import NO_LABEL
from utils.ramps import exp_rampup, pseudo_rampup
from utils.loss import pu_loss

DEBUG = False

class Trainer:

    def __init__(self, model, optimizer, device, config, open_label,
                 writer=None):
        print('using nnPU trainer')
        self.model = model
        self.optimizer = optimizer
        self.save_dir = '{}{}_{}_{}'.format(config.save_dir,
                                            config.arch,
                                            config.dataset,
                                            datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_freq = config.save_freq
        self.print_freq = config.print_freq
        self.ce_loss = torch.nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduction='none')
        self.device = device
        self.writer = writer
        self.open_label = open_label
        self.global_step = 0
        self.epoch = 0
        self.rampup = pseudo_rampup(config.t1, config.t2)
        #self.rampup = exp_rampup(config.weight_rampup)
        self.weight = config.weight
        self.prior = config.prior
        print('prior: ', self.prior)
        
    def _iteration(self, data_loader, print_freq, is_train=True):
        loop_info = defaultdict(list)
        labeled_n, unlabeled_n = 0, 0
        mode = "train" if is_train else "test"
        for batch_idx, (data, targets) in enumerate(data_loader):
            if isinstance(data, list): data = data[0]
            data, targets = data.to(self.device), targets.to(self.device)
            if is_train:
                lmask, umask = targets.ge(0), targets.le(NO_LABEL)
            else:
                umask = targets.eq(self.open_label)
                lmask = 1-umask
            targets[lmask] = 1
            targets[umask] = -1
            lbs, ubs = lmask.float().sum().item(), umask.float().sum().item()
            ## Training Phase
            outputs, feats = self.model(data)
            loss = pu_loss(outputs, targets, self.prior)
            loop_info['loss'].append(loss.item())
            if is_train:
                ##--- backward ---
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            # log info
            with torch.no_grad():
                preds = outputs.sign().squeeze(1).long()
            labeled_n += lbs
            unlabeled_n += ubs
            if is_train:
                acc = targets[lmask].eq(preds[lmask]).float().sum().item()/lbs
            else:
                acc = targets.eq(preds).float().sum().item()/(lbs+ubs)
            loop_info['acc'].append(acc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[{mode}][{batch_idx:<3}]"\
                      f" loss: {loss}\t acc: {acc}")
                #if not is_train:
                #    print(preds)
                for i in preds.unique():
                    print(i.item(), preds.eq(i).sum().item())
        print(f">>>[{mode}]", f"loss: {sum(loop_info['loss'])}",
              f"acc: {sum(loop_info['acc'])/len(loop_info['acc'])}")
        return loop_info

    def train(self, data_loader, print_freq=20):
        self.model.train()
        with torch.enable_grad():
            self._iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        with torch.no_grad():
            self._iteration(data_loader, print_freq, is_train=False)

    def loop(self, epochs, train_data, test_data, scheduler=None, print_freq=-1):
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None:
                scheduler.step()
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, self.print_freq)
            print("------ Testing epochs: {} ------".format(ep))
            self.test(test_data, self.print_freq)
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
