#!coding:utf-8
import torch
from torch import nn
from torch.nn import functional as F
import time, datetime

from utils.dist import *
from pathlib import Path
from utils.data_utils import NO_LABEL
from utils.ramps import exp_rampup
from utils.loss import kl_div_with_logit, mse_with_softmax, sntg_loss

import numpy as np

class Trainer:
    """Only use for open_theta"""

    def __init__(self, model, pi_model, optimizer, loss_fn, device, config):
        self.model = model
        self.pi_model = pi_model
        self.optimizer = optimizer
        self.ce_loss = nn.CrossEntropyLoss(ignore_index=NO_LABEL)
        self.save_dir = '{}{}_{}_{}_{}'.format(config.save_dir,
                                               config.arch,
                                               config.dataset,
                                               config.model,
                                               datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_freq = config.save_freq
        self.print_freq = config.print_freq
        self.device = device
        self.labeled_bs = config.labeled_batch_size
        self.epoch = 0
        self.cons_weight = config.cons_weight
        self.rampup = exp_rampup(config.weight_rampup)
        self.cons_loss = mse_with_softmax
        self.theta = config.theta
        self.sntg = config.sntg

    def train_iteration(self, data_loader, print_freq):
        loop_loss = []
        accuracy1, accuracy2 = [],[]
        labeled_n, lbs = 0, self.labeled_bs
        for batch_idx, (data, targets) in enumerate(data_loader):
            assert len(data)==2
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            targets = targets.to(self.device)
            ##=== student forward pass ===
            outputs, feats = self.model(data)
            outputs1, outputs2 = outputs
            feats1, feats2 = feats
            ##=== class loss ===
            labeled_loss1 = self.ce_loss(outputs1, targets)
            labeled_loss2 = self.ce_loss(outputs2, targets)
            labeled_loss = labeled_loss1 + labeled_loss2
            ##=== backwark ===
            loss = labeled_loss
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            ##=== log info ===
            labeled_n += lbs
            loop_loss.append(loss.item() / len(data_loader))
            acc1     = targets.eq(outputs1.max(1)[1]).sum().item()
            acc2     = targets.eq(outputs2.max(1)[1]).sum().item()
            accuracy1.append(acc1)
            accuracy2.append(acc2)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[train][{batch_idx:<3}]\t labeled: {labeled_loss.item():.3f}\t "\
                    f"loss: {loss.item():.3f}\t Acc1: {acc1/lbs:.3%}\t"\
                    f"Acc2: {acc2/lbs:.3%}")
        print(f">>>[train]loss: {sum(loop_loss):.3f}\t "\
            f"Acc1: {sum(accuracy1)/labeled_n:.3%}\t"\
            f"Acc2: {sum(accuracy2)/labeled_n:.3%}")

        return loop_loss, accuracy1

    def test_iteration(self, data_loader, print_freq):
        loop_loss = []
        accuracy1, accuracy2 = [],[]
        labeled_n = 0
        for batch_idx, (data, targets) in enumerate(data_loader):
            assert len(data)==2
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            targets = targets.to(self.device)
            ##=== student forward pass ===
            outputs, _ = self.model(data)
            outputs1, outputs2 = outputs
            ##=== class loss ===
            labeled_loss1 = self.ce_loss(outputs1, targets)
            labeled_loss2 = self.ce_loss(outputs1, targets)
            labeled_loss = labeled_loss1 + labeled_loss2
            ##=== loss ===
            loss = labeled_loss
            ##=== log info ===
            lbs = data.size(0)
            labeled_n += lbs
            loop_loss.append(loss.item() / len(data_loader))
            acc1     = targets.eq(outputs1.max(1)[1]).sum().item()
            acc2     = targets.eq(outputs2.max(1)[1]).sum().item()
            accuracy1.append(acc1)
            accuracy2.append(acc2)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[test][{batch_idx:<3}]\t labeled: {labeled_loss.item():.3f}\t "\
                    f"loss: {loss.item():.3f}\t Acc1: {acc1/lbs:.3%}\t"\
                    f"Acc2: {acc2/lbs:.3%}")
        print(f">>>[test]loss: {sum(loop_loss):.3f}\t "\
            f"Acc1: {sum(accuracy1)/labeled_n:.3%}\t"\
            f"Acc2: {sum(accuracy2)/labeled_n:.3%}")

        return loop_loss, accuracy1

    def obtain_prediction(self, data_loader):
        self.model.eval()
        preds1, probs1, preds2, probs2, labels = [], [], [], [], []
        for batch_idx, (data, targets) in enumerate(data_loader):
            assert len(data)==2
            data, data1 = data[0].to(self.device), data[1].to(self.device) 
            ##=== forward ===
            outputs, _ = self.model(data)
            outputs1, outputs2 = outputs
            prob1, pred1 = F.softmax(outputs1.detach().cpu(),1).max(1)
            prob2, pred2 = F.softmax(outputs2.detach().cpu(),1).max(1)
            ##===  ===
            labels.append(targets)
            preds1.append(pred1)
            probs1.append(prob1)
            preds2.append(pred2)
            probs2.append(prob2)
        labels = torch.cat(labels)
        preds1 = torch.cat(preds1)
        probs1 = torch.cat(probs1)
        preds2 = torch.cat(preds2)
        probs2 = torch.cat(probs2)
        return labels, ((preds1,probs1), (preds2,probs2))


    def train(self, data_loader, print_freq=20):
        self.model.train()
        self.pi_model.train()
        with torch.enable_grad():
            loss, correct = self.train_iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        self.pi_model.eval()
        with torch.no_grad():
            loss, correct = self.test_iteration(data_loader, print_freq)

    def loop(self, epochs, train_data, test_data, scheduler=None):
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None:
                scheduler.step()
                #print(">>>lr factor: {}".format(scheduler.lr_lambdas[0](ep)))
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, self.print_freq)
            print("------ Testing epochs: {} ------".format(ep))
            self.test(test_data, self.print_freq)
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
