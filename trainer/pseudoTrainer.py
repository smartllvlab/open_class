#!coding:utf-8
import torch
from torch.nn import functional as F

import time, datetime
from pathlib import Path
from collections import defaultdict

from util.datasets import NO_LABEL
from util.ramps import exp_rampup, pseudo_rampup

DEBUG = False

class Trainer:

    def __init__(self, model, optimizer, device, config,
                 writer=None):
        print('using pseudo trainer')
        self.model = model
        self.optimizer = optimizer
        self.save_dir = '{}{}_{}_{}'.format(config.save_dir,
                                            config.arch,
                                            config.dataset,
                                            datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))
        self.save_freq = config.save_freq
        self.ce_loss = torch.nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduction='none')
        self.device = device
        self.writer = writer
        self.global_step = 0
        self.epoch = 0
        self.rampup = pseudo_rampup(config.t1, config.t2)
        #self.rampup = exp_rampup(config.weight_rampup)
        self.weight = config.weight
        
    def _iteration(self, data_loader, print_freq, is_train=True):
        loop_info = defaultdict(list)
        labeled_n, unlabeled_n = 0, 0
        mode = "train" if is_train else "test"
        for batch_idx, (data, targets) in enumerate(data_loader):
            self.global_step += 1
            if isinstance(data, list): data = data[0]
            data, targets = data.to(self.device), targets.to(self.device)
            if is_train:
                lmask, umask = self.decode_targets(targets)
            else:
                lmask = umask = targets.ge(0)
            lbs, ubs = lmask.float().sum().item(), umask.float().sum().item()
            ## Training Phase
            outputs, feats = self.model(data)
            loss = torch.sum(self.ce_loss(outputs[lmask], targets[lmask])) / lbs
            loop_info['lloss'].append(loss.item())
            if is_train:
                ##--- pseudo labels ---
                with torch.no_grad():
                    pseudo_targets = outputs.max(1)[1]
                uloss = torch.sum(self.ce_loss(outputs[umask], pseudo_targets[umask])) / ubs
                uloss *= self.rampup(self.epoch)*self.weight
                loss += uloss; loop_info['uloss'].append(uloss.item())
                ##--- backward ---
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
            # log info
            labeled_n += lbs
            unlabeled_n += ubs
            lacc = targets[lmask].eq(outputs[lmask].max(1)[1]).float().sum().item()
            uacc = targets[umask].eq(outputs[umask].max(1)[1]).float().sum().item()
            loop_info['lacc'].append(lacc)
            loop_info['uacc'].append(uacc)
            if print_freq>0 and (batch_idx%print_freq)==0:
                print(f"[train][{batch_idx:<3}]", self.gen_info(loop_info, lbs, ubs))
        print(">>>[train]", self.gen_info(loop_info, labeled_n, unlabeled_n, False))
        return loop_info

    def train(self, data_loader, print_freq=20):
        self.model.train()
        with torch.enable_grad():
            self._iteration(data_loader, print_freq)

    def test(self, data_loader, print_freq=10):
        self.model.eval()
        with torch.no_grad():
            self._iteration(data_loader, print_freq, is_train=False)

    def loop(self, epochs, train_data, test_data, scheduler=None, print_freq=-1):
        for ep in range(epochs):
            self.epoch = ep
            if scheduler is not None:
                scheduler.step()
            print("------ Training epochs: {} ------".format(ep))
            self.train(train_data, print_freq)
            print("------ Testing epochs: {} ------".format(ep))
            self.test(test_data, print_freq)
            ## save model
            if self.save_freq!=0 and (ep+1)%self.save_freq == 0:
                self.save(ep)

    def decode_targets(self, targets):
        labeled_mask = targets.ge(0)
        unlabeled_mask = targets.le(NO_LABEL)
        targets[unlabeled_mask] = NO_LABEL*targets[unlabeled_mask]-1
        return labeled_mask, unlabeled_mask

    def gen_info(self, info, lbs, ubs, iteration=True):
        ret = []
        for k, val in info.items():
            n = lbs if k[0]=='l' else ubs
            v = val[-1] if iteration else sum(val)
            s = f'{k}: {v/n:.3%}' if k[-1]=='c' else f'{k}: {v:.5f}'
            ret.append(s)
        return '\t'.join(ret)

    def save(self, epoch, **kwargs):
        if self.save_dir is not None:
            model_out_path = Path(self.save_dir)
            state = {"epoch": epoch,
                    "weight": self.model.state_dict()}
            if not model_out_path.exists():
                model_out_path.mkdir()
            save_target = model_out_path / "model_epoch_{}.pth".format(epoch)
            torch.save(state, save_target)
            print('==> save model to {}'.format(save_target))
