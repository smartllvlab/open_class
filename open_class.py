#!coding:utf-8
import os
import random

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.optim import lr_scheduler
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler

import torchvision
import torchvision.transforms as transforms

from utils import datasets
from utils.ramps import exp_warmup
from utils.metrics import evaluation
from utils.data_utils import TransformTwice as twice
from utils.data_utils import TwoStreamBatchSamplerFix, TwoStreamBatchSampler
from utils.data_utils import relabel_dataset_open, relabel_test_dataset_open
from trainer import consTrainer
from trainer import piTrainer
from architectures.arch import arch

from utils.data_utils import NO_LABEL

def create_data_loaders(train_transform, 
                        eval_transform, 
                        datadir,
                        num_classes,
                        config):
    ## construct close-set dict
    #rclose = random.sample(list(range(num_classes)), config.close) 
    rclose = list(range(config.close)) 
    close_set_dict = {rc:i for i, rc in enumerate(rclose)}
    ## set-up two stream dataloader
    if config.twice:
        train_transform = twice(train_transform)
        eval_transform = twice(eval_transform)

    traindir = os.path.join(datadir, config.train_subdir)
    trainset = torchvision.datasets.ImageFolder(traindir, train_transform)
    ## relabel dataset
    if config.labels:
        with open(config.labels) as f:
            labels = dict(line.split(' ') for line in f.read().splitlines())
        labeled_idxs, unlabeled_idxs = relabel_dataset_open(trainset, labels, close_set_dict)
    #assert len(trainset.imgs) == len(labeled_idxs)+len(unlabeled_idxs)
    print("close-set map: ", close_set_dict)
    select_class = {v:k for k,v in trainset.class_to_idx.items()
                        if v in close_set_dict}
    print("close-set class: ", select_class)
    print("total training samples: ", len(trainset.imgs))
    print("labeled samples: ", len(labeled_idxs))
    print("unlabeled samples: ", len(unlabeled_idxs))
    if config.labeled_batch_size < config.batch_size:
        assert len(unlabeled_idxs)>0
        if config.model=='tempens':
            batchSampler = TwoStreamBatchSamplerFix
        else:
            batchSampler = TwoStreamBatchSampler
        batch_sampler = batchSampler(
            unlabeled_idxs, labeled_idxs, config.batch_size, config.labeled_batch_size)
    else:
        sampler = SubsetRandomSampler(labeled_idxs)
        batch_sampler = BatchSampler(sampler, config.batch_size, drop_last=True)
    train_loader = torch.utils.data.DataLoader(trainset,
                                               batch_sampler=batch_sampler,
                                               num_workers=config.workers,
                                               pin_memory=True)

    evaldir = os.path.join(datadir, config.eval_subdir)
    evalset = torchvision.datasets.ImageFolder(evaldir,eval_transform)
    relabel_test_dataset_open(evalset, close_set_dict)
    eval_loader = torch.utils.data.DataLoader(evalset,
                                              batch_size=config.batch_size,
                                              shuffle=False,
                                              num_workers=2*config.workers,
                                              pin_memory=True,
                                              drop_last=False)
    return train_loader, eval_loader, close_set_dict, len(unlabeled_idxs)

def create_loss_fn(config):
    if config.loss == 'mse':
        criterion = nn.mseloss()
    elif config.loss == 'soft':
        # for pytorch 0.4.1 and 1.0.0
        criterion = nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduction='none')
        # for pytorch 0.4.0
        #criterion = nn.CrossEntropyLoss(ignore_index=NO_LABEL, reduce=False)
    return criterion

def create_optim(params, config):
    if config.optim == 'sgd':
        optimizer = optim.SGD(params, config.lr,
                              momentum=config.momentum,
                              weight_decay=config.weight_decay,
                              nesterov=config.nesterov)
    elif config.optim == 'adam':
        optimizer = optim.Adam(params, config.lr)
    return optimizer

def create_lr_scheduler(optimizer, config):
    if config.lr_scheduler == 'cos':
        scheduler = lr_scheduler.CosineAnnealingLR(optimizer,
                                                   T_max=config.epochs,
                                                   eta_min=config.min_lr)
    elif config.lr_scheduler == 'multistep':
        if config.steps=="":
            return None
        scheduler = lr_scheduler.MultiStepLR(optimizer,
                                             milestones=config.steps,
                                             gamma=config.gamma)
    elif config.lr_scheduler == 'exp-warmup':
        lr_lambda = exp_warmup(config.rampup_length,
                               config.rampdown_length,
                               config.epochs)
        scheduler = lr_scheduler.LambdaLR(optimizer,
                                          lr_lambda=lr_lambda)
    elif config.lr_scheduler == 'none':
        scheduler = None
    return scheduler

def test(config):
    if config.dataset == 'cifar10':
        dataset_config = datasets.cifar10()
    elif config.dataset == 'svhn':
        dataset_config = datasets.svhn()
    elif config.dataset == 'fashion':
        dataset_config = datasets.fashion()
    elif config.dataset == 'cinic10':
        dataset_config = datasets.cinic10()
    ret = create_data_loaders(**dataset_config, config=config)
    train_loader, eval_loader, close_set_dict, unlabeled_size = ret
    
    print('-'*50, 'train')
    for (x1, x2), y in train_loader:
        print(x1.size()) 
        print(x2.size()) 
        print(y.size()) 
        print(y) 
        break

    print('-'*50, 'test')
    for (x1, x2), y in eval_loader:
        print(x1.size()) 
        print(x2.size()) 
        print(y.size()) 
        print(y) 
        break

    print(close_set_dict)


def main(config):
    print("pytorch version : {}".format(torch.__version__))
    dataset_config = datasets.load_data[config.dataset]()
    ret = create_data_loaders(**dataset_config, config=config)
    train_loader, eval_loader, close_set_dict, unlabeled_size = ret

    num_classes = len(close_set_dict)+1
    net = arch[config.arch](num_classes)

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    criterion = create_loss_fn(config)
    net = net.to(device)
    optimizer = create_optim(net.parameters(), config)
    scheduler = create_lr_scheduler(optimizer, config)

    ##=== setup trainer ===
    if config.model == 'ema':
        net2 = arch[config.arch](num_classes)
        net2 = net2.to(device)
        trainer = consTrainer.Trainer(net, net2, optimizer, criterion, device, config)
    elif config.model == 'pi':
        trainer = piTrainer.Trainer(net, net, optimizer, criterion, device, config)
    #trainer.loop(config.epochs, train_loader, eval_loader, scheduler=scheduler)

    ##=== main ===
    best_acc, best_f1 = 0., 0.
    for ep in range(config.epochs):
       if scheduler is not None:
           scheduler.step()
       print("------ Training epochs: {} ------".format(ep))
       trainer.train(train_loader, config.print_freq)
       print("------ Testing epochs: {} ------".format(ep))
       trainer.test(eval_loader, config.print_freq)
       print("------ Testing open epochs: {} ------".format(ep))
       labels, preds, probs = trainer.obtain_prediction(eval_loader)
       print("--- labels ---")
       for i in labels.unique():
           print(i, labels.eq(i).sum())
       print("--- preds ---")
       for i in preds.unique():
           print(i, preds.eq(i).sum())
       acc, f1_score = evaluation(labels, preds, len(close_set_dict))
       if f1_score > best_f1:
           best_acc, best_f1 = acc, f1_score
       print(f"[test]Acc: {acc:.3%}\t Open score: {f1_score:.3%}")
       print(f"[best]Acc: {best_acc:.3%}\t Open score: {best_f1:.3%}")
       ## save model
       if config.save_freq!=0 and (ep+1)%config.save_freq == 0:
           trainer.save(ep)


if __name__ == '__main__':
    from util.Config import create_parser
    config = create_parser().parse_args()
    main(config)
