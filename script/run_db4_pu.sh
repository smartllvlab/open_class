#!/usr/bin/bash
# run open_pu + open class(known class + unknown class)
if [ $3 = "cifar10" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db4_pu_cifar10 2>&1 | tee results/$1
elif [ $3 = "cifar100" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db4_pu_cifar100 2>&1 | tee results/$1
else
    echo "No such dataset !"
fi
