#!/usr/bin/bash
# run open_class
#CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_class 2>&1 | tee results/$1

# run open_theta
CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_theta 2>&1 | tee results/$1
