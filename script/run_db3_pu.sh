#!/usr/bin/bash
# run open_pu + known class
if [ $3 = "cifar10" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db3_pu_cifar10 2>&1 | tee results/$1
elif [ $3 = "cifar100" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db3_pu_cifar100 2>&1 | tee results/$1
elif [ $3 = "mnist" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db3_pu_mnist 2>&1 | tee results/$1
elif [ $3 = "svhn" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db3_pu_svhn 2>&1 | tee results/$1
elif [ $3 = "tiny" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db3_pu_tiny 2>&1 | tee results/$1
elif [ $3 = "plus" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db3_pu_cifar10_plus 2>&1 | tee results/$1
else
    echo "No such dataset !"
fi
