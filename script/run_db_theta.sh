#!/usr/bin/bash
# run open_theta
if [ $3 = "cifar10" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db_theta_cifar10 2>&1 | tee results/$1
elif [ $3 = "cifar100" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.cons_db_theta_cifar100 2>&1 | tee results/$1
else
    echo "No such dataset !"
fi
