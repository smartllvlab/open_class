#!/usr/bin/bash
# run open_theta
if [ $3 = "db_sup" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.sup_cons_db_theta_cifar10 2>&1 | tee results/$1
elif [ $3 = "sup" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.sup_cons_theta 2>&1 | tee results/$1
elif [ $3 = "plus" ]; then
    CUDA_VISIBLE_DEVICES=$2 python -m experiments.sup_cons_theta_plus 2>&1 | tee results/$1
else
    echo "No such model !"
fi
