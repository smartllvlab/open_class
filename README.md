# Codes for Dual-branch Detection and Categorization Network(DBDCN) #

Dual-branch detection and categorization network for semi-supervised open-set recognition

### main procedure ###

*open_db3_pu.py* for SVHN, CIFAR-10 and Tiny-Imagenet

*open_db3_pu_plus.py* for CIFAR-10 + CIFAR-100


### model file of DBDCN ###

*trainer/consDB3Trainer.py*

### To run the code ###

```shell
bash ./script/run_db3_pu.sh [log_filename] [gpu_id] [experiment_name]
```

### Directory structure ###

- architectures/ : network architectures
    - arch.py : network architectures configuration file

- data-local/ : data directory
    - images/ : images of datasets
    - labels/ : lists of labeled data
    - workdir/: datasets in original format
    - bin/    : some functional file for processing orignial data

- experiments/ : training configuration files
    - cons\_db3\_pu\_XXXX.py : configuration for XXXX experiment

- script/ : shell for experiments
    - run\_db3\_pu.sh : shell for DBDCN

- trainer/ : model files
    - consDB3Trainer.py : codes for DBDCN

- utils/ : functional files
    - Config.py : hyper-parameter definition
    - datasets.py : dataset configuration
    - data\_utils.py : data reader
    - loss.py : loss definition, including positive-unlabeled(pu) loss
    - ramps.py : ramp up function for loss weight values
